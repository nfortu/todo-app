var uuid = require('uuid/v1')
var express = require('express')
var app = express()

var todos = [
    {id: uuid(), text: "Preparar calse de NodeJs", state: 0},
    {id: uuid(), text: "asd adada dasdasd", state: 0},
    {id: uuid(), text: "Armar TODO app para workshop", state: 0},
]

app.get('/todos', function (req, res) {
  res.json(todos);
});

app.listen(3001, function () {
  console.log('Todos app listening on port 3001!');
});
import request from 'request-promise-native'
import url from 'url'

const host = "http://localhost:3000"

const transport = {
    
    fetchTodos: function() {
        return request({
            uri: url.resolve(host, '/api/todos'),
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response 
        })
    }
}

export default transport;
import React, { Component } from 'react'
import store from '../store'
import uuid from 'uuid/v1'
import S from 'string'

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    padding: 10,
  },
  create: {
      flexGrow: 1,
  },
  filter: {
    fontSize: 12,
  }
}

//Ver problema de scope
let $input

class TodoToolbar extends Component {

    handleKeyDown (e) {
        if (e.keyCode === 13) {
            e.preventDefault(); // Let's stop this event.
            e.stopPropagation(); // Really this time.
            this.addTodo()
        }
    }
    
    addTodo () {
        const text = S($input.value).trim().s
        if (text && text.length > 0) {
            store.addTodo({
                id: uuid(),
                text: text,
                state: 0
            })
            $input.value = ''
        }
    }

    render() {
        return (
            <div style={styles.container}>
                <div style={styles.create}>
                    <input style={{width: 400, marginRight: 10, padding: 5}} placeholder="Nuevo todo"
                        ref={el => $input = el}
                        onKeyDown={(e) => this.handleKeyDown(e)}/>
                    <button type="submit" onClick={this.addTodo}>Crear</button>
                </div>
                <div style={styles.filter}>
                    {/*Mostrar <select onChange={}>
                        <option value="none">Todos</option>
                        <option value="Pendientes">Pendientes</option>
                        <option value="Completados">Completados</option>
                    </select>*/}
                </div>
            </div>
        )
    }
}

export default TodoToolbar
import React, { Component } from 'react'
import Radium from 'radium'
import color from 'color'
import statePending from '../assets/check_off.png'
import stateDone from '../assets/check_on.png'
import store from '../store'
import PropTypes from 'prop-types'

const background = color('#efefef')

const styles = {
  container: {
    marginBottom: 10,
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    background: background.hexString(),
    alignItems: 'center',
    transition: 'background-color 0.5s ease',
    ':hover': {
        background: background.darken(0.05).hexString()
    }
  },
  title: {
      flexGrow: 1,
  },
  icons: {
      badding: 5
  },
  stateIcon: {
      width: 32, height: 32,
      cursor: 'pointer'
  }
}

class Todo extends Component {

    render() {
        
        const getIconSrc = () => {
            return this.props.state == 0 ? statePending : stateDone
        }

        return (
            <div style={styles.container}>
                <div style={styles.title}>{this.props.text}</div>
                <div style={styles.icons}>
                    <img style={styles.stateIcon} 
                        src={getIconSrc()}
                        onClick={(e) => store.toggleState(this.props.id)}/>
                </div>
            </div>
        )
    }
}

export default Radium(Todo);
import React from 'react';
import TodoToolbar from './TodoToolbar';
import TodoList from './TodoList';

const styles = {
    container: {
        flexGrow: 1,
        maxWidth: 500,
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        height: '100%',
    },
    toolbar: {
        background: "#bac5e8",
    },
    body: {
        flexGrow: 1,
        paddingTop: 20,
    }
}

class Main extends React.Component {

  render() {
       return (
          <div style={styles.container}>
              <div style={styles.toolbar}>
                  <TodoToolbar />
              </div>
              <div style={styles.body}>
                  <TodoList />
              </div>
          </div>
      )
  }
}

export default Main;
import React from 'react'
import logo from '../assets/logo.png'

const styles = {
  container: {
    margin: 5,
    fontSize: 22,
    color: '#fff',
  },
  logo: {
    verticalAlign: 'middle',
    marginRight: 5
  }
}

class AppHeader extends React.Component {

  render() {
    return (
      <p style={styles.container}>
        <img  style={styles.logo} 
              src={logo} width="52"/>
        {this.props.title}
      </p>
    )
  }
}

AppHeader.defaultProps = { title: 'Titulo' }

export default AppHeader
import React, { Component } from 'react'
import store from '../store'
import Todo from './Todo'

const styles = {
  container: {
    padding: 10,
  },
  stateIcon: {
      width: 32, height: 32
  }
}

class TodosList extends Component {
    
    constructor (props) {
        super(props)
        this.state = {
            loading: true,
            todos: []
        }
    }

    onComponentDidMount() {
        store.state.subscribe(
            function (state) {
                this.setState(state)
            },
            function (err) {
                console.log(err)
            },
            function () {
                console.log('store completed')
            }
        )
    }
    
    render() {

        const loading = this.state.loading
        const todos = this.state.todos
        
        if (loading) {
            return (<div>Loading</div>)
        } else {
            return (
                <div style={styles.container}>
                    {
                        todos.map(todo => {
                            return <Todo key={todo.id} {...todo} />
                        })
                    }
                </div>
            )
        }
    }
}

export default TodosList
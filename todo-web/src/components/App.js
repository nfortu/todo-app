import React from 'react';
import AppHeader from './AppHeader';
import Main from './Main';

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        height: '100%'
    },
    header: {
        height: 60,
        background: "#5679e8"
    },
    body: {
        flexGrow: 1,
        paddingTop: 60,
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'center'
    }
}

class App extends React.Component {
  render() {
    return (
        <div style={styles.container}>
            <div style={styles.header}>
                <AppHeader />
            </div>
            <div style={styles.body}>
                <Main />
            </div>
        </div>
    )
  }
}

export default App;
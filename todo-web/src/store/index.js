import transport from '../transport'
import uuid from 'uuid/v1'
import Rx from 'rxjs/Rx'

export class TodoStore {
    
    state

    constructor () {
        this.loadTodos = this.loadTodos.bind(this)
        this.state = new Rx.BehaviorSubject({
            todos: [],
            loading: true,
        })
        this.loadTodos()
    }
    
    addTodo = function (todo) {
        this.todos.push(todo)
    }

    toggleState = function(id) {
        const todo = this.todos.find(t => t.id === id)
        if (todo) {
            todo.state = todo.state === 0 ? 1 : 0
        }
    }
    
    loadTodos () {
        
        const state = this.state
        
        state.next({
            loading: true,
            todos: [],
        })

        transport.fetchTodos()
            .then((todos) => {
                state.next({
                    loading: false,
                    todos: todos,
                })
            }).catch((err) => {
                console.log(err)
            }
        )
    }
}

const store = new TodoStore()
export default store